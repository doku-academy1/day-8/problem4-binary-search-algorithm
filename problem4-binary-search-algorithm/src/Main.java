import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        Integer[] arrays = {1, 1, 3, 5, 5, 6, 7};
        Integer number = 9;
        System.out.println(binarySearch(convertArrayToList(arrays), number));
    }

    public static Integer binarySearch(List<Integer> list, Integer number) {
        Integer kiri = 0, kanan = list.size()-1, location = 0, mid = 0;

        while (kiri <= kanan && location == 0) {

            mid = (kiri+kanan)/2;

            if (list.get(mid) < number) {
                kanan = mid-1;
            } else if (list.get(mid) > number) {
                kiri = mid + 1;
            } else {
                location = mid;
            }
        }

        if (location == 0) {
            location = -1;
        }

        return location;
    }

    public static List<Integer> convertArrayToList(Integer[] arrays) {
        List<Integer> list = new ArrayList<>();
        for (int i = 0; i<arrays.length; i++) {
            list.add(arrays[i]);
        }
        return list;
    }
}